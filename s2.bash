#!/bin/bash

read -p  "Enter your last name: " lastname

read -p  "Enter your first name: " firstname

read -p  "Enter your birthdate in yyyy-mm-dd format: " birthdate

userBirthdate=$(date -d "$birthdate" +%Y-%m-%d)
currentBirthdate=$(date +%Y-%m-%d)


getCurrentYear=$(date +%Y)
getCurrentMonth=$(date +%m)
getCurrentDay=$(date +%d)

getUserBirthYear=$(date -d "$userBirthdate" +%Y)
getUserBirthMonth=$(date -d "$userBirthdate" +%m)
getUserBirthDay=$(date -d "$userBirthdate" +%d)

age=$(($getCurrentYear - $getUserBirthYear))
month=$(($getCurrentMonth - $getUserBirthMonth))

if [[ $month -lt 0 || ( $month -eq 0 &&  $currentBirthdate -lt $userBirthdate ) ]]
then
    age=$(( $age - 1))
fi


echo "Hello, $firstname $lastname! You're ${age} years old!"
