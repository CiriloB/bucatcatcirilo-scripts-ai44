#!/bin/bash

read -p  "What is the name of your folder?: " FOLDERNAME

read -p  "How many copies do you want?: " TOTALCOPIES

if [[ -z "$TOTALCOPIES" ]]
then
    mkdir  $FOLDERNAME
elif [[ $TOTALCOPIES -lt 0 ]]
then
    echo "Total copies should be greater than 0"
elif ! [[ "$TOTALCOPIES" =~ ^[0-9]+$ ]]
then
    echo "Total copies should be integer"
elif [[ $TOTALCOPIES -gt 0 ]]
then
    COUNTER=1
    until [ "$COUNTER" -gt "$TOTALCOPIES" ]
    do
        if [[ $COUNTER -eq 1 ]]
        then
            mkdir  $FOLDERNAME
        else
            mkdir  $FOLDERNAME$COUNTER
        fi
        ((COUNTER++))
    done
    echo "Folder(s) created"
fi




